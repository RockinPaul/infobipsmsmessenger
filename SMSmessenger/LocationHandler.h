//
//  LocationHandler.h
//  SMSmessenger
//
//  Created by Pavel on 01.11.15.
//  Copyright © 2015 infobip. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@interface LocationHandler : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;

- (double)getUserLatitude;
- (double)getUserLongitude;
+ (LocationHandler *)sharedInstance;

@end
