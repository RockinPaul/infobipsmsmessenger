//
//  RESTclient.m
//  SMSmessenger
//
//  Created by Pavel on 01.11.15.
//  Copyright © 2015 infobip. All rights reserved.
//

#import "RESTclient.h"

@implementation RESTclient

NSString *const base_url = @"https://api.infobip.com/";
NSString *const sender = @"InfoSMS";
NSString *const username = @"RockinPaul";
NSString *const password = @"temppassword1";

#pragma mark - Send message

- (void)sendSMSwithText:(NSString *)text To:(NSString *)recipient {
    
    NSData *nsdata = [[NSString stringWithFormat:@"%@:%@", username, password]
                      dataUsingEncoding:NSUTF8StringEncoding];
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:[NSString stringWithFormat:@"Basic %@", base64Encoded] forHTTPHeaderField:@"Authorization"];
    self.manager.requestSerializer = requestSerializer;
    
    NSString *completeURL = [NSString stringWithFormat:@"%@sms/1/text/single", base_url];
    [self.manager POST:completeURL
       parameters:@{
                    @"from":sender,
                    @"to":recipient,
                    @"text":text
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"success" object:self];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            self.errorDescription = [error localizedDescription];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"fail" object:self];
        }];
}


#pragma mark - User location

- (void)createSMSforRecipient:(NSString *)recipient withText:(NSString *)text {
    
    LocationHandler *locationHanlder = [LocationHandler sharedInstance];
    double lat = [locationHanlder getUserLatitude];
    double lon = [locationHanlder getUserLongitude];
    
    NSString *completeURL = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true", lat, lon];
    [self.manager POST:completeURL
            parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSArray *responseArray = [responseObject objectForKey:@"results"];
                NSString *addressString = @"";
                if (responseArray.count != 0) {
                    NSArray *addressJson = [responseArray[0] objectForKey:@"address_components"];
                    for (NSDictionary *component in addressJson) {
                        addressString = [addressString stringByAppendingString:[component objectForKey:@"long_name"]];
                        addressString = [addressString stringByAppendingString:@","];
                    }
                } else {
                    addressString = @"Address is not defined.";
                }
                NSString *message = [NSString stringWithFormat:@"%@. %@", text, addressString];
                [self sendSMSwithText:message To:recipient];
                
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"ERROR: %@", [error localizedDescription]);
        }];
}


#pragma mark - Shared Instance

+ (RESTclient *)sharedInstance {
    static dispatch_once_t pred;
    static RESTclient *sharedInstance = nil;
    [[NSURLCache sharedURLCache] removeAllCachedResponses]; // Remove all cached responses :) TODO: NEED TO DEACTIVATE CACHE ONLY FOR SPECIFIC REQUESTS!
    dispatch_once(&pred, ^ {
        sharedInstance = [[RESTclient alloc] init];
        sharedInstance.manager = [AFHTTPRequestOperationManager manager];
        AFJSONResponseSerializer *jsonReponseSerializer = [AFJSONResponseSerializer serializer];
        // This will make the AFJSONResponseSerializer accept any content type.
        jsonReponseSerializer.acceptableContentTypes = nil;
        sharedInstance.manager.responseSerializer = jsonReponseSerializer;
    });
    return sharedInstance;
}

@end
