//
//  LocationHandler.m
//  SMSmessenger
//
//  Created by Pavel on 01.11.15.
//  Copyright © 2015 infobip. All rights reserved.
//

#import "LocationHandler.h"

@implementation LocationHandler

#pragma mark - user latitude

- (double)getUserLatitude {
    return self.locationManager.location.coordinate.latitude;
}


#pragma mark - user longtitude

- (double)getUserLongitude {
    return self.locationManager.location.coordinate.longitude;
}


#pragma mark - initialization

+ (LocationHandler *)sharedInstance {
    static dispatch_once_t pred;
    static LocationHandler *sharedInstance = nil;
    dispatch_once(&pred, ^ {
        sharedInstance = [[LocationHandler alloc] init];
        sharedInstance.locationManager = [[CLLocationManager alloc] init];
        sharedInstance.locationManager.delegate = sharedInstance;
        sharedInstance.locationManager.distanceFilter = kCLDistanceFilterNone;
        sharedInstance.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            [sharedInstance.locationManager requestWhenInUseAuthorization];
        }
        [sharedInstance.locationManager startUpdatingLocation];
    });
    return sharedInstance;
}

@end
