//
//  AppDelegate.h
//  SMSmessenger
//
//  Created by Pavel on 29.10.15.
//  Copyright © 2015 infobip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

