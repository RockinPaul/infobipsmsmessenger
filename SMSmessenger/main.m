//
//  main.m
//  SMSmessenger
//
//  Created by Pavel on 29.10.15.
//  Copyright © 2015 infobip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
