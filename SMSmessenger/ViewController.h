//
//  ViewController.h
//  SMSmessenger
//
//  Created by Pavel on 29.10.15.
//  Copyright © 2015 infobip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESTclient.h"

@interface ViewController : UIViewController <UITextViewDelegate, UITextFieldDelegate>

FOUNDATION_EXPORT float const buttonWidth;
FOUNDATION_EXPORT float const buttonHeight;

@property (nonatomic, strong) UITextField *numberTextField;
@property (nonatomic, strong) UITextView *messageTextView;

@end

