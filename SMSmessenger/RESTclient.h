//
//  RESTclient.h
//  SMSmessenger
//
//  Created by Pavel on 01.11.15.
//  Copyright © 2015 infobip. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "LocationHandler.h"

@interface RESTclient : NSObject

FOUNDATION_EXPORT NSString *const base_url;
FOUNDATION_EXPORT NSString *const sender;
FOUNDATION_EXPORT NSString *const username;
FOUNDATION_EXPORT NSString *const password;

@property (nonatomic, strong) AFHTTPRequestOperationManager *manager;
@property (nonatomic, strong) NSString *errorDescription;

- (void)createSMSforRecipient:(NSString *)recipient withText:(NSString *)text;
+ (RESTclient *)sharedInstance;

@end
