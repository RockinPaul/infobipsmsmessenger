//
//  ViewController.m
//  SMSmessenger
//
//  Created by Pavel on 29.10.15.
//  Copyright © 2015 infobip. All rights reserved.
//

#import "ViewController.h"


@implementation ViewController

float const buttonWidth = 100;
float const buttonHeight = 40;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor grayColor];
    
    self.numberTextField = [[UITextField alloc] initWithFrame:CGRectMake(10, 40, self.view.frame.size.width - 20, 30)];
    self.numberTextField.delegate = self;
    self.numberTextField.placeholder = @"Phone number";
    self.numberTextField.keyboardType = UIKeyboardTypePhonePad;
    self.numberTextField.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.numberTextField];
    
    self.messageTextView = [[UITextView alloc] initWithFrame:CGRectMake(10, 90, self.view.frame.size.width - 20, self.view.frame.size.height/3)];
    self.messageTextView.delegate = self;
    [self.view addSubview:self.messageTextView];
    
    UIButton *sendButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 50, self.messageTextView.frame.size.height + 95, 100, 40)];
    sendButton.backgroundColor = [UIColor blueColor];
    [sendButton setTitle:@"Send" forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(sendButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:sendButton];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(observeSuccess)
                                                 name:@"success"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(observeError)
                                                 name:@"fail"
                                               object:nil];
}


#pragma mark - Send button pressed

- (void)sendButtonPressed {
    
    UIAlertController *alertController = nil;
    NSString *number = self.numberTextField.text;
    NSString *message = nil;
    
    if (![self isValidPhone:number]) {
        alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Invalid phone number" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDestructive handler:nil];
        [alertController addAction:alertAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else if (![self.messageTextView hasText]) {
        alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Message is empty" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDestructive handler:nil];
        [alertController addAction:alertAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        message = self.messageTextView.text;
        [[RESTclient sharedInstance] createSMSforRecipient:number withText:message];
    }
}


#pragma mark - UITextView delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
                                                replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView endEditing:YES];
    }
    return YES;
}


#pragma mark - UITextField delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return YES;
}


#pragma mark - Dismiss keyboard on tap

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}


#pragma mark - Phone validation

- (BOOL)isValidPhone:(NSString *)phoneNumber {
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phoneNumber];
}


#pragma mark - Observe error

- (void)observeError {
    NSString *errorMessage = [NSString stringWithFormat:@"Sending failed: %@", [RESTclient sharedInstance].errorDescription];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDestructive handler:nil];
    [alertController addAction:alertAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Observe success

- (void)observeSuccess {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Message sent" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:alertAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
